'use strict';
var cHelper = require('./');
var _ = require('lodash');
var Baobab = require('baobab');

console.log('\n\nTest Set A');

var qualityAlert = new Baobab({
	id: 0,
	location: 1,
	supplier: 2,
	locationInfo: [{
		id: 0,
		qualityAlert: 0,
		location: 1,
		supplier: 2,
		agreement: 0,
		platform: 'Platform name',
		discrepantMaterial: [
			{
				id: 0,
				part: 0,
				supplier: 2,
				qualityAlert: 0
			}
		]
	}]
});

console.log('-------- Test 1: Adding to existing array. ----------------');
var materialCursor = cHelper.select(qualityAlert, ['locationInfo', {location: 1}, 'discrepantMaterial']);
cHelper.add(materialCursor, null, {part: 999});
console.log(JSON.stringify(qualityAlert.get(), null, '\t'));

console.log('-------- Test 2: Removing an item from array ----------------');
cHelper.remove(materialCursor, {part: 999});
console.log(JSON.stringify(qualityAlert.get(), null, '\t'));

console.log('-------- Test 3: Removing an array ----------------');
cHelper.remove(materialCursor);
console.log(JSON.stringify(qualityAlert.get(), null, '\t'));

console.log('-------- Test 4: Adding to non-existing array. ----------------');
var materialCursor = cHelper.select(qualityAlert, ['locationInfo', {location: 1}, 'discrepantMaterial'], {create: true});
cHelper.add(materialCursor, null, {part: 999});
console.log(JSON.stringify(qualityAlert.get(), null, '\t'));


console.log('\n\nTest Set B');
var qualityAlert = new Baobab({
	id: 0,
	location: 1,
	supplier: 2
});

console.log('-------- Test 1: Adding to non-existing deep path. ----------------');
var materialCursor = cHelper.select(qualityAlert, ['locationInfo', {location: 1}, 'discrepantMaterial'], {create: true});
cHelper.add(materialCursor, null, {part: 999});
console.log(JSON.stringify(qualityAlert.get(), null, '\t'));


console.log('-------- Test 2: Save to existing path. ----------------');
var materialCursor = cHelper.select(qualityAlert, ['locationInfo', {location: 1}, 'discrepantMaterial']);
cHelper.save(materialCursor, {part: 999}, {supplier: 1});
console.log(JSON.stringify(qualityAlert.get(), null, '\t'));


console.log('-------- Test 3: Save to non-existing path. ----------------');
var materialCursor = cHelper.select(qualityAlert, ['locationInfo', {location: 1}, 'discrepantMaterial']);
cHelper.save(materialCursor, {part: 123}, {supplier: 1});
console.log(JSON.stringify(qualityAlert.get(), null, '\t'));



console.log('\n\nTest Set C');

var qualityAlert = new Baobab({
	id: 0,
	location: 1,
	supplier: 2,
	locationInfo: [{
		id: 0,
		qualityAlert: 0,
		location: 1,
		supplier: 2,
		agreement: 0,
		platform: 'Platform name',
		discrepantMaterial: [
			{
				id: 0,
				part: 0,
				supplier: 2,
				qualityAlert: 0
			},
			{
				id: 0,
				part: 1,
				supplier: 2,
				qualityAlert: 0
			}
		]
	}]
});


console.log('-------- Test 1: Get deep path. ----------------');
var value = cHelper.get(qualityAlert, ['locationInfo', {location: 1}, 'discrepantMaterial', {part: 1}], 'nothing');
console.log(JSON.stringify(value, null, '\t'));

console.log('-------- Test 2: Get array. ----------------');
var value = cHelper.get(qualityAlert, ['locationInfo', {location: 1}, 'discrepantMaterial'], 'nothing');
console.log(JSON.stringify(value, null, '\t'));

console.log('-------- Test 3: Get non-existing WITHOUT default value. ----------------');
var value = cHelper.get(qualityAlert, ['locationInfo', {location: 1}, 'discrepantMaterialAAAA']);
console.log(JSON.stringify(value, null, '\t'));

console.log('-------- Test 4: Get non-existing with default value. ----------------');
var value = cHelper.get(qualityAlert, ['locationInfo', {location: 1}, 'discrepantMaterialAAAA'], 'nothing');
console.log(JSON.stringify(value, null, '\t'));

console.log('-------- Test 5: Get null path. ----------------');
var value = cHelper.get(qualityAlert, ['locationInfo', {location: null}, 'discrepantMaterial'], 'nothing');
console.log(JSON.stringify(value, null, '\t'));
