'use strict';
var _ = require('lodash');
/* TODO optimize stepTraverse
    if (_.isArray(key)) {
        //Check if all items in array are primitive (string or number)
        var allPrim = true;
        _.forEach(key, function(aKey){
            if (_.isString(aKey) == false && _.isNumber(aKey) == false) {
                //TODO make sure not float
                allPrim = false;
                return false;
            }
        });

        if (allPrim) {
            //Try to traverse
            setCursor = cursor.select(key);
            break;
        }
    } else if(_.isObject(key)) {

    } else {
        //Normal primitive key

    }
*/

function stepTraverse(cursor, path, options) {
    /*
        cursor, key
        cursor, [key1, key2, key3]
        cursor, {id: x}
        cursor, [key1, {id: x}, key2]
    */
    if (_.isArray(path) == false) {
        path = [path];
    }
    var currentCursor = cursor;
    for(var i=0, len=path.length; i<len; i++) {
        var step = path[i];
        var lastCursor = currentCursor;
        if (_.isArray(step)) {
            //Error, can't have an array as a step ie. [key1, [], key2]
            throw 'Step "'+JSON.stringify(step)+'" not preformed, cannot have an array as a step in path.';
        } else if (_.isObject(step)) {
            //Do search at current cursor
            //Current cursor should be an array of objects
            var list = currentCursor.get();
            if (_.isArray(list) == false) {
                if (options.create == true && list == null) {
                    //Create array at path
                    currentCursor.set([]);
                } else {
                    throw 'Step "' + JSON.stringify(step) + '" not preformed, cursor is not an array.';
                }
            }
            var index = _.findIndex(list, step);
            if (index == -1) {
                if (options.create == true) {
                    //Create path, index in array doesn't exist, push a new object
                    var newIndex = currentCursor.get().length;
                    currentCursor.push(step);
                    //May need a tree.commit here
                    currentCursor = currentCursor.select(newIndex);
                } else {
                    throw 'Step "' + JSON.stringify(step) + '" not preformed, path does not exist and options.create == false.';
                }
            } else {
                //Traverse
                currentCursor = currentCursor.select(index);
            }
        } else {
            //Step is integer, string, or null
            if (step == null) {
                throw 'Step "'+JSON.stringify(step)+'" not preformed, step cannot be null if first step.';
            } else if (currentCursor.exists(step) == false) {
                //Can't traverse
                if (options.create == true) {
                    var nextStep = path[i+1];
                    //Create path for step
                    if (_.isUndefined(nextStep)) {
                        //If no next step, set to null
                        currentCursor.set(step, null);
                    } else if (_.isString(nextStep)) {
                        //If next step is a string, create an object
                        currentCursor.set(step, {});
                    } else if (_.isObject(nextStep)) {
                        //If next step is an object, create an array, and add keys
                        currentCursor.set(step, [nextStep]);
                    } else {
                        //If next step is an integer create an array
                        currentCursor.set(step, []);
                    }
                    //Traverse to new creation
                    currentCursor = currentCursor.select(step);
                } else {
                    throw 'Step "'+JSON.stringify(step)+'" not preformed, path does not exist and options.create == false.';
                }
            } else {
                //Traverse
                currentCursor = currentCursor.select(step);
            }
        }
    }
    return currentCursor;
}

function _select(cursor, path, options) {
    var defaultOptions = {
        create: false
    };
    var theOptions = _.merge({}, defaultOptions, options);
    return stepTraverse(cursor, path, theOptions);
}

function _add(cursor, selection, value) {
    /*
    Add to array or replace object or primitive with value
        If cursor + selection
            is array: push value to array, return index
            is null: create array with value inside
            is object: replace object with value
            is primitive: replace primitive with value
    */
    if (selection != null) {
        cursor = stepTraverse(cursor, selection);
    }
    var curVal = cursor.get();
    if (_.isArray(curVal)) {
        var index = curVal.length;
        cursor.push(value);
        cursor.tree.commit();
        return index;
    } else if (curVal == null) {
        cursor.set([value]);
    } else if (_.isObject(curVal)) {
        cursor.set(value);
    } else {
        cursor.set(value);
    }
    cursor.tree.commit();
}
function _remove(cursor, selection) {
    /*
    Remove array, object, or primitive
        If cursor + selection
            is array: unset array
            is null: nothing
            is object: unset object
            is primitive: unset primitive
    */
    if (selection != null) {
        cursor = stepTraverse(cursor, selection, {create: false});
    }
    var curVal = cursor.get();
    if (curVal != null) {
        cursor.unset();
    }
    cursor.tree.commit();
}
function _save(cursor, selection, value) {
    /*
    Update object (merge) or add value to array, or replace primitive with value
        If cursor + selection (create selection if not there)
            is array: push value to array
            is null: If value is obj create array with obj inside, else replace with value
            is object: merge object with value, or if value primitive replace obj
            is primitive: replace primitive wiht value
    */
    if (selection != null) {
        cursor = stepTraverse(cursor, selection, {create: true});
    }
    var curVal = cursor.get();
    if (_.isArray(curVal)) {
        cursor.push(value);
    } else if (curVal == null) {
        cursor.set(value);
    } else if (_.isObject(curVal)) {
        if (_.isObject(value)) {
            cursor.merge(value);
        } else {
            cursor.set(value);
        }
    } else {
        cursor.set(value);
    }
    cursor.tree.commit();
}
function _update(cursor, selection, value) {
    /*
    Update object (merge) or replace primitive
        If cursor + selection
            is array: error
            is null: replace with value
            is object: merge object with value, or if value primitive replace obj
            is primitive: replace primitive with value
    */
    if (selection != null) {
        cursor = stepTraverse(cursor, selection);
    }
    var curVal = cursor.get();
    if (_.isArray(curVal)) {
        throw '_update: cannot update an array. Must select item in array.';
    } else if (curVal == null) {
        cursor.set([value]);
    } else if (_.isObject(curVal)) {
        if (_.isObject(value)) {
            cursor.merge(value);
        } else {
            cursor.set(value);
        }
    } else {
        cursor.set(value);
    }
    cursor.tree.commit();
}
function _get(cursor, selection, defaultValue) {
    if (cursor == null) {
        return defaultValue;
    }
    if (selection != null) {
        try {
            cursor = stepTraverse(cursor, selection); 
        } catch (e) {
            return defaultValue;
        }
    }
    return cursor.get() || defaultValue;
}
function _getIndex(cursor, selection, defaultValue) {
    if (cursor == null) {
        return defaultValue;
    }
    if (selection != null) {
        try {
            cursor = stepTraverse(cursor, selection); 
        } catch (e) {
            return defaultValue;
        }
        cursor = cursor.up();
        var index = _.findIndex(cursor.get(), _.last(selection));
        if (index == -1) {
            return defaultValue;
        } else {
            return index;
        }
    }
    return defaultValue;
}
module.exports = {
    select: _select,
    add: _add,
    remove: _remove,
    save: _save,
    update: _update,
    get: _get,
    getIndex: _getIndex
};


